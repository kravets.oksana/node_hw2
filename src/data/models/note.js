const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
  text: { type: String, required: true },
  userId: {
    type: mongoose.SchemaTypes.ObjectId,
    required: true,
    ref: 'user',
  },
  completed: {
    type: Boolean,
  },
  createdDate: {
    type: Date,
    default: () => Date.now(),
    immutable: true,
  },
  updatedDate: { type: Date, default: () => Date.now() },
});

noteSchema.pre('save', function (next) {
  this.updatedDate = Date.now();
  next();
});

module.exports = mongoose.model('Note', noteSchema);
