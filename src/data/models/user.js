const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  username: { type: String, required: true },
  password: { type: String, required: true },
  createdDate: {
    type: Date,
    default: () => Date.now(),
    immutable: true,
  },
  updatedDate: { type: Date, default: () => Date.now() },
});

userSchema.pre('save', function (next) {
  this.updatedDate = Date.now();
  next();
});

module.exports = mongoose.model('User', userSchema);
