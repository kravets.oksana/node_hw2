module.exports = {
  UserRepository: require('./users'),
  NotesRepository: require('./notes'),
};
