const models = require('../models/models');

class User {
  user = models.User;

  async create(userData) {
    const newUser = await this.user.create(userData);
    return newUser;
  }

  async getByUsername(username) {
    const user = await this.user.findOne({ username });
    return user;
  }

  async getUserById(id) {
    const user = await this.user.findById(id);
    return user;
  }

  async userExists(username, password) {
    const userExists = await this.user.exists({
      username,
    });
    return userExists;
  }

  async deleteUser(id) {
    await this.user.findByIdAndDelete(id);
  }

  async updateUser(param, value, id) {
    const userToUpdate = await this.user
      .findById(id)
      .select();

    userToUpdate[param] = value;
    await userToUpdate.save();
  }
}

module.exports = User;
