const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const repositories = require('../../data/repositories/repositories');

const userRepository = new repositories.UserRepository();

const secret = 'secret_key';

module.exports.createUser = async (req, res, next) => {
  const { username, password } = req.body;

  const hashPass = await bcrypt.hash(password, 10);
  try {
    await userRepository.create({
      username,
      password: hashPass,
    });
    res.status(200).send({ message: 'Success' });
  } catch (e) {
    res.status(500).send(e.message);
  }
  next();
};

module.exports.loginUser = async (req, res, next) => {
  const { username, password } = req.body;

  try {
    const userExists = await userRepository.userExists(
      username,
    );

    if (userExists) {
      const user = await userRepository.getByUsername(
        username,
      );
      if (
        await bcrypt.compare(
          String(password),
          String(user.password),
        )
      ) {
        const payload = {
          username: user.username,
          userId: user.id,
        };
        const token = jwt.sign(payload, secret);
        res.status(200).json({  message: "Success", jwt_token: token });
      }
    } else {
      res
        .status(400)
        .send({ message: 'User does not exist' });
    }
  } catch (e) {
    res.status(500).send(e.message);
  }
  next();
};

module.exports.auth = (req, res, next) => {
  const { authorization } = req.headers;

  try {
    if (!authorization) {
      return res.status(401).json({
        message: 'Please, provide authorization header',
      });
    }

    const [, token] = authorization.split(' ');

    if (!token) {
      return res.status(401).json({
        message: 'Please, include token to request',
      });
    }

    try {
      const tokenPayload = jwt.verify(token, secret);
      req.user = {
        userId: tokenPayload.userId,
        username: tokenPayload.username,
      };
    } catch (err) {
      return res.status(401).json({ message: err.message });
    }
  } catch (e) {
    res.status(500).send(e.message);
  }

  next();
};
