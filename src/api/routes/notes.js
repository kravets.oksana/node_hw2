const express = require('express');
const authMiddleware = require('../services/auth');
const notesMiddleware = require('../services/notes');

const router = express.Router();

router.get(
  '/',
  [authMiddleware.auth, notesMiddleware.getAll],
);

router.post(
  '/',
  [authMiddleware.auth, notesMiddleware.createNote],
);

router.get(
  '/:id',
  [authMiddleware.auth, notesMiddleware.getById],
);
router.put(
  '/:id',
  [authMiddleware.auth, notesMiddleware.updateById],
);

router.patch(
  '/:id',
  [authMiddleware.auth, notesMiddleware.updateStatus],
);

router.delete(
  '/:id',
  [authMiddleware.auth, notesMiddleware.delete],
);

module.exports = router;
