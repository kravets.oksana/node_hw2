const express = require('express');
const authMiddleware = require('../services/auth');
const userMiddleware = require('../services/users');

const router = express.Router();

router.get(
  '/me',
  [authMiddleware.auth, userMiddleware.getUser],
);

router.delete(
  '/me',
  [authMiddleware.auth, userMiddleware.deleteUser],
);
router.patch(
  '/me',
  [authMiddleware.auth, userMiddleware.updateUser],
);

module.exports = router;
