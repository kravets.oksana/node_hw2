const express = require('express');

const router = express.Router();

const services = require('../services/auth');

router.post(
  '/register',
  services.createUser,
);
router.post('/login', services.loginUser);

module.exports = router;
